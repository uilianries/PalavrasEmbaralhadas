# Coursera

## Orientação a Objetos com Java

### Exercício 6

#### Instructions

#### Jogo das Palavras Embaralhadas
O objetivo desse exercício é criar um jogo onde partes de sua execução possam ser trocadas. 

O jogo é simples: é apresentado ao jogador uma palavra com as letras embaralhadas e o jogador deve
tentar adivinhar a palavra correta. O jogo será jogado no próprio console e a lista de palavras utilizadas
pode ser fixa (pelo menos 20).
