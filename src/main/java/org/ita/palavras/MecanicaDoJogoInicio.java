package org.ita.palavras;

/**
 * Interface que representa o inicio do Jogo.
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */


/**
 * Representação do estado inicial
 */
public class MecanicaDoJogoInicio implements MecanicaDoJogo {
    /**
     * Retorna o proximo estado a ser executado
     * @return Proximo estado a ser executado
     */
    public MecanicaDoJogo proximoEstado() {
        return new MecanicaDoJogoTentativa();
    }

    /**
     * Executa a tarefa do estado
     * @param contexto  Contexto de execução
     */
    public void executa(Contexto contexto) {
        contexto.pontuacao = 0;
        Teclado.escrever("== Palavras Embaralhadas ==");
    }
}
