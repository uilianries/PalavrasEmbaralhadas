package org.ita.palavras;

/**
 * Interface que representa o final do Jogo.
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */


/**
 * Representação do estado final
 */
public class MecanicaDoJogoFinal implements MecanicaDoJogo {
    /**
     * Retorna o proximo estado a ser executado
     * @return Proximo estado a ser executado
     */
    public MecanicaDoJogo proximoEstado() {
        return null;
    }

    /**
     * Executa a tarefa do estado
     * @param contexto  Contexto de execução
     */
    public void executa(Contexto contexto) {
        String resultado = "== Palavras Embaralhadas - RESULTADO ==\n" +
                "Dificuldade: " + contexto.dificuldade.name() + "\n" +
                "Pontos: " + contexto.pontuacao;
        Teclado.escrever(resultado);
    }
}
