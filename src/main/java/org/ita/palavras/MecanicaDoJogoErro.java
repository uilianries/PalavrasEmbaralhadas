package org.ita.palavras;

/**
 * Interface que representa o erro do jogador.
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */


/**
 * Representação do estado de erro
 */
public class MecanicaDoJogoErro implements MecanicaDoJogo {
    /** Finalizar jogo */
    private boolean finalizar = false;

    /**
     * Retorna o proximo estado a ser executado
     * @return Proximo estado a ser executado
     */
    public MecanicaDoJogo proximoEstado() {
        MecanicaDoJogo mecanicaDoJogo;
        if (finalizar) {
            mecanicaDoJogo = new MecanicaDoJogoFinal();
        } else {
            mecanicaDoJogo = new MecanicaDoJogoTentativa();
        }
        return mecanicaDoJogo;
    }

    /**
     * Executa a tarefa do estado
     * @param contexto  Contexto de execução
     */
    public void executa(Contexto contexto) {
        contexto.tentativas--;
        Teclado.escrever("Erro! Tentativas restantes: " + contexto.tentativas);
        if (contexto.tentativas == 0) {
            this.finalizar = true;
        }
    }
}
