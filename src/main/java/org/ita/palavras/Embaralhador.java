package org.ita.palavras;

/**
 * Interface que representa classes reponsáveis
 * por receber uma palavra e retornar ela embaralhada
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */


/**
 * Interface embaralhadora
 */
public interface Embaralhador {

    /**
     * Embaralha uma determinada palavra e retorna a mesma
     * @param palavra      Palavra que deve ser embaralhada
     * @return palavra embaralhada
     */
    public String embaralhar(final String palavra);
}
