package org.ita.palavras;

/**
 * Possui um método que retorna um embaralhador aleatóriamente
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
import java.util.ArrayList;


/**
 * Factory de objeto embaralhador
 */
public class FabricaEmbaralhadores {

  /**
   * Cria um embaralhador aleatorio
   * @return Instancia do embaralhador criada
   */
  public static Embaralhador criar() {
      ArrayList<Embaralhador> embaralhadores = new ArrayList<Embaralhador>() {{
          add(new EmbaralhadorReverso());
          add(new EmbaralhadorOrdenado());
          add(new EmbaralhadorAleatorio());
      }};
      int indice = Sorteio.gerar(embaralhadores.size());
      return embaralhadores.get(indice);
  }
}
