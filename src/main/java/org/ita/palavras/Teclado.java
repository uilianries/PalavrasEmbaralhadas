package org.ita.palavras;

import java.util.Scanner;

/**
 * Representação de teclado através do console
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */

/**
 * Helper para funções de teclado
 */
public class Teclado {
    /**
     * Realiza leitura de entrada
     * @return Próxima linha lida no STDIN
     */
    private static Scanner scanner = new Scanner(System.in);

    public static String ler() {
        return scanner.nextLine();
    }

    /**
     * Escreve uma mensagem no STDOUT
     * @param mensagem  Mensagem que deverá ser escrita no console
     */
    public static void escrever(final String mensagem) {
        System.out.println(mensagem);
    }
}
