package org.ita.palavras;

/**
 * classe que possui um método que retorna
 * uma palavra retirada aleatóriamente de uma
 * lista de palavras lida a partir de um arquivo
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Logger;
import java.util.logging.Level;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Carrega um arquivo numa lista de palavras
 */
public class BancoDePalavras {
    /** Nome do arquivo de configuração */
    private static final String ARQUIVO_CONFIG = "src/main/conf/config.json";
    /** Instancia do arquivo carregado */
    JSONObject arquivoJson = null;

    /**
     * Carrega o arquivo de configuração
     * @param arquivo  Nome do arquivo
     */
    private BancoDePalavras(final String arquivo) {
        JSONParser jsonParser = new JSONParser();
        try {
            FileReader fileReader = new FileReader(arquivo);
            Object object = (Object) jsonParser.parse(fileReader);
            this.arquivoJson = (JSONObject) object;
        } catch (FileNotFoundException exception) {
            abortar("Não foi possível abrir o banco de dados", exception);
        } catch (IOException exception) {
            abortar("Não pode parsear arquivo JSON", exception);
        } catch (ParseException exception) {
            abortar("Não pode parsear arquivo JSON", exception);
        }
    }

    /**
     * Busca palavra aleatoria no json
     * @param nivel  Nivel de dificuldade da palavra
     * @return Palavra sorteada
     */
    private String buscaPalavraJson(Dificuldade nivel) {
        String dificuldade = nivel.name().toLowerCase();
        JSONArray array = (JSONArray) this.arquivoJson.get(dificuldade);
        return (String) array.get(Sorteio.gerar(array.size()));
    }

    /**
     * Envia mensagem de erro e finaliza processo
     * @param  mensagem  Mensagem de log
     */
    private void abortar(String mensagem, Exception exception) {
        final Logger logger = Logger.getLogger(getClass().getName());
        logger.log(Level.SEVERE, mensagem, exception);
        System.exit(1);
    }

    /**
     * Abre o banco e carrega uma palavra, de acordo com o nivel
     * @param nivel  Nivel de dificuldade da palavra
     * @return Palavra para jogar
     */
    static public String recuperaPalavra(Dificuldade nivel) {
        BancoDePalavras banco = new BancoDePalavras(BancoDePalavras.ARQUIVO_CONFIG);
        return banco.buscaPalavraJson(nivel);
    }
}
