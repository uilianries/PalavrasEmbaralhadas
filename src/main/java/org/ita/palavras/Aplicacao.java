package org.ita.palavras;

/**
 * Representação do cerne do programa
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
import org.apache.commons.cli.*;

import java.lang.IllegalArgumentException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Aplicação de jogo
 */
public class Aplicacao {
    /** Contexto do jogo */
    private Contexto contexto = new Contexto();

    /**
     * Parser de argumentos da entrada
     */
    public Aplicacao(String [] args) {
        tratarArgumentos(args);
    }

    /**
     * Inicia o jogo
     */
    public void executar() {
        MecanicaDoJogo estado = FabricaMecanicaDoJogo.criar();

        do {
            estado.executa(contexto);
            estado = estado.proximoEstado();
        } while (estado != null);
    }

    private void tratarArgumentos(String[] args) {
        final String opcaoAjuda = "ajuda";
        final String opcaoDificuldade = "dificuldade";
        Options options = new Options();
        options.addOption("a", opcaoAjuda, false, "Ajuda sobre o jogo");
        options.addOption("d", opcaoDificuldade, false, "Seleciona dificuldade [facil|regular|dificil]");

        CommandLineParser parser = new GnuParser();
        CommandLine commandLine;

        try {
            commandLine = parser.parse(options, args);

            if (commandLine.hasOption(opcaoAjuda))             {
                String ajuda = "Jogo Palavras Embaralhadas\n" +
                        "O jogo é simples: é apresentado ao jogador uma palavra\n"
                        + " com as letras embaralhadas e o jogador deve tentar\n"
                        + " adivinhar a palavra correta";

                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp(ajuda, options);
                System.exit(0);
            }

            if (commandLine.hasOption(opcaoDificuldade)) {
                final HashMap<String, Dificuldade> dificuldade = new HashMap<String, Dificuldade>() {{
                    put("facil", Dificuldade.FACIL);
                    put("regular", Dificuldade.REGULAR);
                    put("dificil", Dificuldade.DIFICIL);
                }};

                String valor = commandLine.getOptionValue(opcaoDificuldade);
                if (!dificuldade.containsKey(valor.toLowerCase())) {
                    throw new IllegalArgumentException(Arrays.toString(args));
                }
                this.contexto.dificuldade = dificuldade.get(valor.toLowerCase());
            }

        } catch (ParseException exception) {
            final Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Não pode tratar entrada de argumentos", exception);
            System.exit(1);
        } catch (IllegalArgumentException exception) {
            final Logger logger = Logger.getLogger(getClass().getName());
            logger.log(Level.SEVERE, "Argumento passado como inválido", exception);
            System.exit(1);
        }
    }
}
