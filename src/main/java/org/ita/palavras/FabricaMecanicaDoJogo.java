package org.ita.palavras;

/**
 * Retorna a MecanicaDoJogo que deve ser utilizada.
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */


/**
 * Factory pattern
 */
public class FabricaMecanicaDoJogo {

    /**
     * Retorna o proximo estado a ser executado
     * @return Instancia do estado
     */
    static public MecanicaDoJogo criar() {
        return new MecanicaDoJogoInicio();
    }
}
