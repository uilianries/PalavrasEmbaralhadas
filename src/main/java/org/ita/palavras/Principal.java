package org.ita.palavras;

/**
 * Representação do inicio do programa
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */


/**
 * Execução do jogo
 */
public class Principal {

    public static void main(String [] args) {
        Aplicacao app = new Aplicacao(args);
        app.executar();
    }
}
