package org.ita.palavras;

/**
 * Implementa interface Embaralhador utilizando
 * String ordenada
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
import java.util.Arrays;


/**
 * Embaralha palavras
 */
public class EmbaralhadorOrdenado implements Embaralhador {

    /**
     * Embaralha uma determinada palavra e retorna a mesma
     * @param palavra      Palavra que deve ser embaralhada
     * @return palavra embaralhada com string reversa
     */
    public String embaralhar(final String palavra) {
        char[] array = palavra.toCharArray();
        Arrays.sort(array);
        return new String(array);
    }
}
