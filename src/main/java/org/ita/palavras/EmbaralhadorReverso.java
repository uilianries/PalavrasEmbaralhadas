package org.ita.palavras;

/**
 * Implementa interface Embaralhador utilizando
 * String invertida
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
import java.lang.StringBuffer;


/**
 * Embaralha palavras
 */
public class EmbaralhadorReverso implements Embaralhador {

    /**
     * Embaralha uma determinada palavra e retorna a mesma
     * @param palavra      Palavra que deve ser embaralhada
     * @return palavra embaralhada com string reversa
     */
    public String embaralhar(final String palavra) {
        return new StringBuffer(palavra).reverse().toString();
    }
}
