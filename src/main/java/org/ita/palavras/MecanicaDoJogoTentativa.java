package org.ita.palavras;

/**
 * Interface que representa um tentativa do Jogo.
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */


/**
 * Representação do estado de tentaviva
 */
public class MecanicaDoJogoTentativa implements MecanicaDoJogo {
    /** */
    boolean correto = false;

    /**
     * Retorna o proximo estado a ser executado
     * @return Proximo estado a ser executado
     */
    public MecanicaDoJogo proximoEstado() {
        MecanicaDoJogo mecanicaDoJogo;
        if (!this.correto) {
            mecanicaDoJogo = new MecanicaDoJogoErro();
        } else {
            mecanicaDoJogo = new MecanicaDoJogoTentativa();
        }

        return mecanicaDoJogo;
    }

    /**
     * Executa a tarefa do estado
     * @param contexto  Contexto de execução
     */
    public void executa(Contexto contexto) {
        String palavra = BancoDePalavras.recuperaPalavra(contexto.dificuldade);
        Embaralhador embaralhador = FabricaEmbaralhadores.criar();
        String embaralhada = embaralhador.embaralhar(palavra);

        Teclado.escrever("Adivinhe a seguinte palvra: " + embaralhada);
        String entrada = Teclado.ler();
        this.correto = entrada.equalsIgnoreCase(palavra);
        if (this.correto) {
            contexto.pontuacao += 5 * (contexto.dificuldade.ordinal() + 1);
            Teclado.escrever("Correto! Nova pontuação: " + contexto.pontuacao);
        }
    }
}
