package org.ita.palavras;

/**
 * Realiza o sorteio de numeros
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */

import java.util.Random;

/**
 * Realiza o sortio de um numero
 */
public class Sorteio {

    /**
     * Gera um numero aleatorio
     * @param limite  Numero limite para sorteio
     * @return Numero sorteado entre e incluindo 0 - limite
     */
    public static int gerar(int limite) {
        Random random = new Random();
        return random.nextInt(limite);
    }
}
