package org.ita.palavras;

/**
 * Implementa interface Embaralhador utilizando
 * permutação
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
import java.util.Collections;
import java.util.Arrays;
import java.util.List;

/**
 * Embaralha palavras
 */
public class EmbaralhadorAleatorio implements Embaralhador {

    /**
     * Embaralha uma determinada palavra e retorna a mesma
     * @param palavra      Palavra que deve ser embaralhada
     * @return palavra embaralhada com string reversa
     */
    public String embaralhar(final String palavra) {
        List<String> letras = Arrays.asList(palavra.split(""));
        Collections.shuffle(letras);
        // Java 8 possui String.join()
        String embaralhada = "";
        for (String letra : letras) {
            embaralhada += letra;
        }
        return embaralhada;
    }
}
