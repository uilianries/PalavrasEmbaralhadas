package org.ita.palavras;

/**
 * Contexto de compartilhamento entre estados
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */

import java.util.Map;

/**
 * Contexto geral
 */
public class Contexto {
    /** Pontuação atual do jogador */
    public int pontuacao = 0;
    /** Dificuldade do jogo */
    public Dificuldade dificuldade = Dificuldade.REGULAR;
    /** Tentativas restantes */
    public int tentativas = 3;
}
