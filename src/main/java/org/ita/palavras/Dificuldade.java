package org.ita.palavras;

/**
 * Representação de dificuldade de resolução
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */


/**
 * Níveis do jogo
 */
public enum Dificuldade { FACIL, REGULAR, DIFICIL };
