package org.ita.palavras;

/**
 * Interface que representa o andamento e a lógica do jogo.
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */


/**
 * Representação de pseudo máquina de estados
 */
public interface MecanicaDoJogo {

    /**
     * Retorna o proximo estado a ser executado
     * @return Proximo estado a ser executado
     */
    public MecanicaDoJogo proximoEstado();

    /**
     * Executa a tarefa do estado
     * @param contexto  Contexto de execução
     */
    public void executa(Contexto contexto);
}
