package org.ita.palavras;

/**
 * Implementa teste unitário para a classe
 * BancoDePalavras
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */

import java.util.Arrays;
import org.junit.Test;
import static org.junit.Assert.assertEquals;


/**
 * Valida Embaralha palavras
 */
public class TestEmbaralhador {
    /** Palavras para entrada do teste */
    private static final String [] PALAVRAS = { "niobio", "magnesio", "tungstenio" };

    /**
     * Ordena uma string
     * @param palavra  Palavra para ser orenada
     * @return String orenada
     */
    private static String sort(final String palavra) {
        char[] array = palavra.toCharArray();
        Arrays.sort(array);
        return new String(array);
    }

    /**
     * Executa processo de validação sobre instancia
     * @param embaralhador  Instancia de embaralhador para ser validado
     */
    private void validaEntradas(Embaralhador embaralhador) {
        for (String palavra : PALAVRAS) {
            String embaralhada = embaralhador.embaralhar(palavra);

            assertEquals(palavra.length(), embaralhada.length());
            assertEquals(sort(palavra), sort(embaralhada));
        }
    }

    /**
     * Valida entradas contra EmbaralhadorAleatorio
     */
    @Test
    public void testEmbaralhadorAletorio() {
        Embaralhador embaralhador = new EmbaralhadorAleatorio();
        validaEntradas(embaralhador);
    }

    /**
     * Valida entradas contra EmbaralhadorReverso
     */
    @Test
    public void testEmbaralhadorReverso() {
        Embaralhador embaralhador = new EmbaralhadorReverso();
        validaEntradas(embaralhador);
    }

    /**
     * Valida entradas contra EmbaralhadorOrdenado
     */
    @Test
    public void testEmbaralhadorOrdenado() {
        Embaralhador embaralhador = new EmbaralhadorOrdenado();
        validaEntradas(embaralhador);
    }

}
