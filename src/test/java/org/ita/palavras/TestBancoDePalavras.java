package org.ita.palavras;

/**
 * Implementa teste unitário para a classe
 * EmbaralhadorAleatorio
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */
import org.junit.Test;
import static org.junit.Assert.assertTrue;
import java.nio.file.Paths;

/**
 * Valida carga de arquivos
 */
public class TestBancoDePalavras {

    /**
     * Valida palavra busca de palavra facil
     */
    @Test
    public void testRecuperaPalavraFacil() {
        String palavra = BancoDePalavras.recuperaPalavra(Dificuldade.FACIL);
        assertTrue(!palavra.isEmpty());
    }

    /**
     * Valida palavra busca de palavra regular
     */
    @Test
    public void testRecuperaPalavraRegular() {
        String palavra = BancoDePalavras.recuperaPalavra(Dificuldade.REGULAR);
        assertTrue(!palavra.isEmpty());
    }

    /**
     * Valida palavra busca de palavra dificil
     */
    @Test
    public void testRecuperaPalavraDificil() {
        String palavra = BancoDePalavras.recuperaPalavra(Dificuldade.DIFICIL);
        assertTrue(!palavra.isEmpty());
    }
}
