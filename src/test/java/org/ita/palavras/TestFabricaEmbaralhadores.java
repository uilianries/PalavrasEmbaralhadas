package org.ita.palavras;

/**
 * Implementa teste unitário para a classe FabricaEmbaralhadores
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */

import org.junit.Test;
import static org.junit.Assert.assertTrue;


/**
 * Valida FabricaEmbaralhadores
 */
public class TestFabricaEmbaralhadores {

    /**
     * Valida geração de embaralhadores
     */
    @Test
    public void testCriar() {
        for (int i = 0; i < 10; i++) {
            Embaralhador embaralhador = FabricaEmbaralhadores.criar();
            assertTrue(embaralhador != null);
        }
    }
}
