package org.ita.palavras;

/**
 * Implementa teste unitário para a classe Sorteio
 *
 * @author Uilian Ries <uilianries@gmail.com>
 */

import org.junit.Test;
import static org.junit.Assert.assertTrue;


/**
 * Valida Sorteio
 */
public class TestSorteio {

    /**
     * Valida entradas contra Sorteio
     */
    @Test
    public void testGerar() {
        for (int i = 1; i < 100; i++) {
            int bingo = Sorteio.gerar(i);
            assertTrue(bingo >= 0 && bingo <= i);
        }
    }
}
